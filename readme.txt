Payment Tracker (by Jakub Starek)
..............................................
HOMEWORK FOR BACKEND DEVELOPER POSITION IN BSC
-----------------------------------------------

o  Prerequisites
    - JDK 8
    - Maven 3
    - Git

o How to build and run the app
    - clone paymenttracker to your local machine
    - go to paymenttracker directory and run command "mvn package";
      this will produce payment-tracker-1.0.jar file in ./target folder
    - run payment-tracker-1.0.jar using "java -jar <path>/payment-tracker-1.0.jar"
        * e.g.: "java -jar target\payment-tracker-1.0.jar" (on Windows)
    - you can optionally run application with 1 argument; the argument is a path to a file with payments that shall be imported;
      template of this file is shown in payments.txt (it's path can be used as an argument straightaway)
          * e.g.: "java -jar target\payment-tracker-1.0.jar payments.txt" (on Windows)
          * you'll be notify about import result before the application bootstraps
          * NOTE: application can have at most 1 argument!

o How to use the app
    - after application bootstrap you can enter your commands
    - you can enter a valid payment command, i.e. currency code followed by space followed by amount
        * e.g.: "USD 500" or "EUR -500"
        * currency code must exist (see Assumptions chapter)
        * currency code could be entered uppercase, lowercase or their combination
        * amount could be positive or negative
        * command with amount 0 (e.g. CZK 0) is considered invalid (explained in Assumptions chapter)
        * after successful processed command, you'll be notified about it
    - if you enter invalid or empty command, application won't do anything and notify you about command invalidity
    - to exit the application just enter word "quit"
    - every 60 seconds application will display net amount of each currency; in case that net amount is 0, entry doesn't display
    - in case of application failure, you can see application.log file

o Assumptions
    - submission: "Data should be kept in memory (please don’t introduce any database engines)"
    - assumption: Situation: user runs app with "path to batch file" argument. I originally wanted to make this job asynchronous.
            Because of "do not use any DB engines" I gave up this idea. Reason is that I wanted to have consistent data and
            I'm not familiar how to implement transactions on Map properly. So application enables command typing after batch file import.
            In case of unsuccessful import, Map for data holding is empty.

    - submission: "The input can be typed into the command line with possibility to be automated in the future"
    - assumption: I didn't understand what "possibility to be automated in the future" means in this context.

    - submission: "the currency may be any uppercase 3 letter code, such as USD, HKD, RMB, NZD, GBP etc."
    - assumption: I assumed that currency code should exist; therefore program validates it.

    - no submission for command with zero amount
    - assumption: My assumption is to consider this kind of command invalid. It would do nothing anyway ;-).

    - no submission for unsuccessful processing
    - assumption:
        * what quits the program
            • wrong number of arguments when starting it (only 0 or 1 is allowed)

        * what doesn't quit the program
            • wrong input argument: path to non-existing or invalid file; results application start with empty "database".
            • invalid or empty command; results no change in "database".
            • unsuccessful load of net amounts; instead of displaying net amounts program notifies you it was unable to get them.


    - no requirement for storing individual payments
    - assumption: Net amounts are only data we need to store for the purpose of the application.
            Therefore individual payments are not stored. They are only included in net amounts.

o Used frameworks
    The application is based on Spring Boot framework. For unit tests I've used JUnit and Mockito framework.

o Optional submissions
    - Unit testing: partially done - very poor coverage; just one test class has been implemented to demonstrate I can use JUnit and Mockito
    - Threadsafe code: done - hopefully making PaymentInMemoryDAO methods synchronized is enough for this program
    - Documentation: skipped
    - Programming patterns: done - Chain of responsibility pattern used for CommandHandler
    - "Optional bonus question": skipped