package com.jst.payment_tracker.impl.dao;

import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.dao.entity.Payment;
import com.jst.payment_tracker.api.excepion.DAOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.class)
public class PaymentInMemoryDAOTest {

    private PaymentInMemoryDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new PaymentInMemoryDAO();
        dao.init();
    }

    @Test
    public void executeSinglePayment_ok() throws DAOException {
        dao.executeSinglePayment(new Payment("USD",300));
        Map<String, Long> paymentData = dao.getPaymentData();
        assertEquals(1, paymentData.keySet().size());
        assertEquals(300, paymentData.get("USD").longValue());
    }

    @Test(expected = DAOException.class)
    public void executeSinglePayment_error() throws DAOException {
        Payment payment = new Payment("CZK",500);
        dao = mock(PaymentInMemoryDAO.class);
        doThrow(RuntimeException.class).when(dao).appendEntry(payment);
        doCallRealMethod().when(dao).executeSinglePayment(payment);
        dao.executeSinglePayment(payment);
    }

    @Test
    public void executePaymentBulk_ok() throws DAOException {
        List<Payment> payments = Arrays.asList(
                new Payment("USD", 50),
                new Payment("CZK",200),
                new Payment("USD", -200));
        dao.executePaymentBulk(payments);
        Map<String, Long> paymentData = dao.getPaymentData();
        assertEquals(2, paymentData.keySet().size());
        assertEquals(-150, paymentData.get("USD").longValue());
        assertEquals(200, paymentData.get("CZK").longValue());
    }

    @Test(expected = DAOException.class)
    public void executePaymentBulk_error() throws DAOException {
        Payment payment = new Payment("CZK",500);
        List<Payment> payments = Arrays.asList(
                payment,
                new Payment("CZK",200),
                new Payment("USD", -200));
        dao = mock(PaymentInMemoryDAO.class);
        doThrow(RuntimeException.class).when(dao).appendEntry(payment);
        doCallRealMethod().when(dao).executePaymentBulk(payments);
        dao.executePaymentBulk(payments);
    }

    @Test
    public void getNetAmounts_ok() throws DAOException {
        List<Payment> payments = Arrays.asList(
                new Payment("USD", 50),
                new Payment("CZK",200),
                new Payment("USD", -200));
        dao.executePaymentBulk(payments);
        List<CurrencyBalance> netAmounts = dao.getNetAmounts();
        assertEquals(2, netAmounts.size());
        assertEquals("CZK", netAmounts.get(0).getCurrency());
        assertEquals(200, netAmounts.get(0).getBalance());
        assertEquals("USD", netAmounts.get(1).getCurrency());
        assertEquals(-150, netAmounts.get(1).getBalance());
    }
}