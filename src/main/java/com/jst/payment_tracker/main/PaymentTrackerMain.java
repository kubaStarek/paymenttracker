package com.jst.payment_tracker.main;

import com.jst.payment_tracker.api.console.ConsoleApp;
import com.jst.payment_tracker.impl.config.MainConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@Import(MainConfig.class)
@PropertySource("classpath:application_text.properties")
public class PaymentTrackerMain implements CommandLineRunner{

    private static final Logger logger = LoggerFactory.getLogger(PaymentTrackerMain.class);

    private static final String APP_ERROR_KEY = "txt.main.error";

    @Autowired
    private Environment env;

    @Autowired
    private ConsoleApp consoleApp;

    public static void main(String[] args) {
        new SpringApplicationBuilder(PaymentTrackerMain.class)
                .bannerMode(Banner.Mode.LOG)
                .web(WebApplicationType.NONE)
                .run(args);
    }

    @Override
    public void run(String... args) {
        try {
            consoleApp.run(args);
        } catch (Throwable t){
            System.out.println(env.getProperty(APP_ERROR_KEY));
            logger.error("Unexpected error occurred", t);
            System.exit(-1);
        }
    }
}
