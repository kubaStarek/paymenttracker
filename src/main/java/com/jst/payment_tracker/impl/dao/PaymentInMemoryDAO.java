package com.jst.payment_tracker.impl.dao;

import com.jst.payment_tracker.api.dao.PaymentDAO;
import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.dao.entity.Payment;
import com.jst.payment_tracker.api.excepion.DAOException;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class PaymentInMemoryDAO implements PaymentDAO {

    private Map<String, Long> paymentData;

    @PostConstruct
    protected void init(){
        paymentData = new HashMap<>();
    }

    @Override
    public synchronized void executeSinglePayment(Payment payment) throws DAOException {
        try {
            appendEntry(payment);
        } catch (Exception e){
            throw new DAOException("Error during single payment execution.", e);
        }
    }

    @Override
    public synchronized void executePaymentBulk(List<Payment> payments) throws DAOException {
        try {
            payments.forEach(p -> appendEntry(p));
        } catch (Exception e){
            throw new DAOException("Error during bulk payment execution.", e);
        }
    }

    @Override
    public synchronized List<CurrencyBalance> getNetAmounts() throws DAOException {
        try{
            return paymentData.entrySet().stream()
                    .filter(x -> x.getValue() != 0)
                    .sorted(Map.Entry.comparingByKey())
                    .map(x -> new CurrencyBalance(x.getKey(), x.getValue()))
                    .collect(Collectors.toList());
        } catch (Exception e){
            throw new DAOException("Error during getting Net Amounts.", e);
        }
    }

    synchronized void appendEntry(Payment payment){
        Long previousValue = paymentData.get(payment.getCurrency());
        paymentData.put(payment.getCurrency(), previousValue == null
                ? payment.getAmount()
                : previousValue + payment.getAmount());
    }

    synchronized Map<String, Long> getPaymentData() {
        return paymentData;
    }

}
