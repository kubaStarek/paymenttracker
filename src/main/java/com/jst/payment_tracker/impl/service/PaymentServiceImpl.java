package com.jst.payment_tracker.impl.service;

import com.jst.payment_tracker.api.dao.PaymentDAO;
import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.dao.entity.Payment;
import com.jst.payment_tracker.api.excepion.DAOException;
import com.jst.payment_tracker.api.excepion.ServiceException;
import com.jst.payment_tracker.api.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import static com.jst.payment_tracker.api.excepion.ServiceException.ErrorStatus.GENERAL_ERROR;
import static com.jst.payment_tracker.api.excepion.ServiceException.ErrorStatus.INVALID_CURRENCY;
import static com.jst.payment_tracker.api.excepion.ServiceException.ErrorStatus.INVALID_INPUT;
import static com.jst.payment_tracker.api.excepion.ServiceException.ErrorStatus.INVALID_INPUT_FILE;
import static com.jst.payment_tracker.impl.util.Util.parsePaymentLine;


@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

    private PaymentDAO paymentDAO;

    public PaymentServiceImpl(PaymentDAO paymentDAO) {
        this.paymentDAO = paymentDAO;
    }

    @Override
    public void executeSinglePayment(String currency, long amount) throws ServiceException {
        if(!isCurrencyValid(currency))
            throw new ServiceException(INVALID_CURRENCY);
        try{
            paymentDAO.executeSinglePayment(new Payment(currency,amount));
        }catch (DAOException e){
            logger.error("Error during single payment execution.", e);
            throw new ServiceException(GENERAL_ERROR);
        }
    }

    @Override
    public void importPaymentFile(String filename) throws ServiceException {
        try{
            paymentDAO.executePaymentBulk(parseFile2PaymentList(filename));
        }catch (DAOException e){
            logger.error("Error during payment file import.", e);
            throw new ServiceException(GENERAL_ERROR);
        }
    }

    private List<Payment> parseFile2PaymentList(String filename) throws ServiceException {
        List<Payment> payments = new ArrayList<>();
        try(Scanner scanner = new Scanner(new File(filename))){
            while (scanner.hasNextLine()) {
                Optional<String[]> currencyAmountTuple = parsePaymentLine(scanner.nextLine());
                if(!currencyAmountTuple.isPresent())
                    throw new ServiceException(INVALID_INPUT);
                if(!isCurrencyValid(currencyAmountTuple.get()[0]))
                    throw new ServiceException(INVALID_CURRENCY);
                payments.add(
                        new Payment(currencyAmountTuple.get()[0], Long.parseLong(currencyAmountTuple.get()[1])));

            }
        }catch (FileNotFoundException e){
            throw new ServiceException(INVALID_INPUT_FILE);
        }
        return payments;
    }

    @Override
    public List<CurrencyBalance> getNetAmounts() throws ServiceException {
        try {
            return paymentDAO.getNetAmounts();
        } catch (DAOException e) {
            logger.error("Error during getting Net Amounts.", e);
            throw new ServiceException(GENERAL_ERROR, "Unable to get Net Amounts.", e);
        }
    }

    private boolean isCurrencyValid(String s) {
        try {
            Currency.getInstance(s);
        } catch (IllegalArgumentException e){
            return false;
        }
        return true;
    }

}
