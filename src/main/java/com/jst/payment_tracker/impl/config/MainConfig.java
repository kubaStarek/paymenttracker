package com.jst.payment_tracker.impl.config;

import com.jst.payment_tracker.api.console.ConsoleApp;
import com.jst.payment_tracker.api.console.processor.ArgsProcessor;
import com.jst.payment_tracker.api.dao.PaymentDAO;
import com.jst.payment_tracker.api.service.PaymentService;
import com.jst.payment_tracker.impl.console.PaymentTrackerConsoleApp;
import com.jst.payment_tracker.impl.console.handler.CommandHandler;
import com.jst.payment_tracker.impl.console.processor.SimpleArgsProcessor;
import com.jst.payment_tracker.impl.dao.PaymentInMemoryDAO;
import com.jst.payment_tracker.impl.job.ScheduledJobs;
import com.jst.payment_tracker.impl.service.PaymentServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(CommandHandlerConfig.class)
public class MainConfig {

    @Bean
    public ConsoleApp consoleApp(CommandHandler mainCH, ArgsProcessor argsProcessor){
        return new PaymentTrackerConsoleApp(mainCH, argsProcessor);
    }

    @Bean
    public PaymentDAO paymentDAO(){
        return new PaymentInMemoryDAO();
    }

    @Bean
    public PaymentService paymentService(PaymentDAO paymentDAO){
        return new PaymentServiceImpl(paymentDAO);
    }

    @Bean
    public ArgsProcessor argsProcessor(PaymentService paymentService){
        return new SimpleArgsProcessor(paymentService);
    }

    @Bean
    public ScheduledJobs scheduledJobs(PaymentService paymentService){
        return new ScheduledJobs(paymentService);
    }
}
