package com.jst.payment_tracker.impl.config;

import com.jst.payment_tracker.api.service.PaymentService;
import com.jst.payment_tracker.impl.console.handler.CommandHandler;
import com.jst.payment_tracker.impl.console.handler.PaymentCommandHandler;
import com.jst.payment_tracker.impl.console.handler.builder.HandlerChainBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandHandlerConfig {

    private static final String EMPTY_COMMAND_KEY = "txt.command.handler.empty";
    private static final String QUIT_COMMAND_KEY = "txt.command.handler.quit";

    @Bean("mainCH")
    public CommandHandler mainCommandHandler(PaymentService paymentService){
        return (new HandlerChainBuilder<CommandHandler>())
            .add(emptyCommandHandler())
            .add(quitCommandHandler())
            .add(paymentCommandHandler(paymentService))
            .build();
    }


    @Bean("emptyCH")
    public CommandHandler emptyCommandHandler() {
        return new CommandHandler() {
            @Override
            public void process(String request) {
            if(Strings.isEmpty(request))
                System.out.println(env.getProperty(EMPTY_COMMAND_KEY));
            else
                proceedToNext(request);
            }
        };
    }


    @Bean("quitCH")
    public CommandHandler quitCommandHandler(){
        return new CommandHandler() {
            @Override
            public void process(String request) {
                if("quit".equals(request)) {
                    System.out.println(env.getProperty(QUIT_COMMAND_KEY));
                    System.exit(0);
                } else {
                    proceedToNext(request);
                }
            }
        };
    }

    @Bean("paymentCH")
    public CommandHandler paymentCommandHandler(PaymentService paymentService){
        return new PaymentCommandHandler(paymentService);
    }
}
