package com.jst.payment_tracker.impl.console;

import com.jst.payment_tracker.api.console.ConsoleApp;
import com.jst.payment_tracker.api.console.processor.ArgsProcessor;
import com.jst.payment_tracker.impl.console.handler.CommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PaymentTrackerConsoleApp implements ConsoleApp {

    private static final String APP_GREETING_KEY = "txt.main.greeting";

    @Autowired
    private Environment env;

    private CommandHandler commandHandler;

    private ArgsProcessor argsProcessor;

    public PaymentTrackerConsoleApp(CommandHandler commandHandler, ArgsProcessor argsProcessor) {
        this.commandHandler = commandHandler;
        this.argsProcessor = argsProcessor;
    }

    @Override
    public void run(String... args) {
        argsProcessor.process(args);
        System.out.println(env.getProperty(APP_GREETING_KEY));
        try(Scanner scanner = new Scanner(System.in)){
            while (true) {
                commandHandler.process(scanner.nextLine());
            }
        }
    }
}
