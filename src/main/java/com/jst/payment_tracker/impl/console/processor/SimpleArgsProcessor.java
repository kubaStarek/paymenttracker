package com.jst.payment_tracker.impl.console.processor;

import com.jst.payment_tracker.api.console.processor.ArgsProcessor;
import com.jst.payment_tracker.api.excepion.ServiceException;
import com.jst.payment_tracker.api.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SimpleArgsProcessor implements ArgsProcessor {

    private static final String WRONG_ARG_COUNT_KEY = "txt.arg.pcs.wrong.arg.count";
    private static final String PI_INIT_KEY = "txt.arg.pcs.payment.import.init";
    private static final String PI_OK_KEY = "txt.arg.pcs.payment.import.successful";
    private static final String PI_ERR_INVALID_FILE_KEY = "txt.arg.pcs.payment.import.error.invalid.file";
    private static final String PI_ERR_INVALID_INPUT_KEY_KEY = "txt.arg.pcs.payment.import.error.invalid.input";
    private static final String PI_ERR_INVALID_CURRENCY_KEY = "txt.arg.pcs.payment.import.error.invalid.currency";
    private static final String PI_ERR_GENERAL_KEY = "txt.arg.pcs.payment.import.error.general";
    private static final String DELIMITER_KEY = "txt.delimiter";

    @Autowired
    private Environment env;

    private PaymentService paymentService;

    public SimpleArgsProcessor(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void process(String... args) {
        switch (args.length){
            case 0:
                break;
            case 1:
                processPaymentFile(args[0]);
                break;
            default:
                System.out.println(env.getProperty(WRONG_ARG_COUNT_KEY));
                System.exit(1);
        }
    }

    private void processPaymentFile(String filename) {
        System.out.println(env.getProperty(PI_INIT_KEY)+" " + filename);
        try {
            paymentService.importPaymentFile(filename);
            System.out.println(env.getProperty(PI_OK_KEY));
        } catch (ServiceException e) {
            resolveServiceExceptionStatus(e.getErrorStatus());
        }
        System.out.println(env.getProperty(DELIMITER_KEY));
    }

    private void resolveServiceExceptionStatus(ServiceException.ErrorStatus errorStatus){
        switch (errorStatus){
            case INVALID_INPUT_FILE:
                System.out.println(env.getProperty(PI_ERR_INVALID_FILE_KEY));
                break;
            case INVALID_INPUT:
                System.out.println(env.getProperty(PI_ERR_INVALID_INPUT_KEY_KEY));
                break;
            case INVALID_CURRENCY:
                System.out.println(env.getProperty(PI_ERR_INVALID_CURRENCY_KEY));
                break;
            default:
                System.out.println(env.getProperty(PI_ERR_GENERAL_KEY));
        }
    }
}
