package com.jst.payment_tracker.impl.console.handler;

import com.jst.payment_tracker.api.console.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public abstract class CommandHandler implements Handler<String> {

    private static final String INVALID_COMMAND_KEY = "txt.command.handler.invalid";

    @Autowired
    protected Environment env;

    private CommandHandler nextHandler;

    protected void proceedToNext(String request){
        if(nextHandler == null)
            System.out.println(env.getProperty(INVALID_COMMAND_KEY));
        else
            nextHandler.process(request);
    }

    @Override
    public void setNextHandler(Handler<String> handler) {
        if(handler instanceof CommandHandler)
            nextHandler = (CommandHandler) handler;
        else
            throw new IllegalArgumentException("Handler param must be instance of "+getClass().getName());
    }
}
