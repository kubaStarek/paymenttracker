package com.jst.payment_tracker.impl.console.handler;

import com.jst.payment_tracker.api.excepion.ServiceException;
import com.jst.payment_tracker.api.service.PaymentService;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.jst.payment_tracker.impl.util.Util.parsePaymentLine;

@Component
public class PaymentCommandHandler extends CommandHandler {

    private static final String PAYMENT_OK_KEY = "txt.command.handler.payment.ok";
    private static final String PAYMENT_ERR_KEY = "txt.command.handler.payment.error";
    private static final String PAYMENT_INVALID_CURRENCY_KEY = "txt.command.handler.payment.error.invalid.currency";

    private PaymentService paymentService;

    public PaymentCommandHandler(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void process(String request) {
        Optional<String[]> parsedPaymentInput = parsePaymentLine(request);
        if(parsedPaymentInput.isPresent()){
            executePayment(parsedPaymentInput.get());
        } else {
            proceedToNext(request);
        }
    }

    private void executePayment(String[] currency_amount){
        try {
            paymentService.executeSinglePayment(currency_amount[0].toUpperCase(),
                   Long.parseLong(currency_amount[1]));
            System.out.println(env.getProperty(PAYMENT_OK_KEY));
        } catch (ServiceException e) {
            resolveServiceExceptionStatus(e.getErrorStatus());
        }
    }

    private void resolveServiceExceptionStatus(ServiceException.ErrorStatus errorStatus){
        switch (errorStatus){
            case INVALID_CURRENCY:
                System.out.println(env.getProperty(PAYMENT_INVALID_CURRENCY_KEY));
                break;
            default:
                System.out.println(env.getProperty(PAYMENT_ERR_KEY));
        }
    }
}
