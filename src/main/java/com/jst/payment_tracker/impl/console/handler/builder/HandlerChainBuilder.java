package com.jst.payment_tracker.impl.console.handler.builder;

import com.jst.payment_tracker.api.console.handler.Handler;

public class HandlerChainBuilder<T extends Handler> {
    private T first;
    private T current;

    public HandlerChainBuilder<T> add(T ch){
        if(first == null){
            first = ch;
        } else {
           current.setNextHandler(ch);
        }
        current = ch;
        return this;
    }

    public T build(){
        return first;
    }
}
