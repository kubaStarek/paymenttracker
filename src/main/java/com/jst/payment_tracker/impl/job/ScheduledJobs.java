package com.jst.payment_tracker.impl.job;

import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.excepion.ServiceException;
import com.jst.payment_tracker.api.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScheduledJobs {

    private static final String NET_AMOUNTS_ERROR_KEY = "txt.job.netamounts.error";
    private static final String NET_AMOUNTS_STATEMENT_TEMPLATE_KEY = "txt.job.netamounts.statement.template";

    @Autowired
    private Environment env;

    private PaymentService paymentService;

    public ScheduledJobs(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Scheduled(fixedRateString="${job.scheduled.reportCurrencyBalance.fixRate}")
    protected void reportCurrencyBalance() {
        try {
            printNetAmounts(paymentService.getNetAmounts());
        } catch (ServiceException e) {
            System.out.println(env.getProperty(NET_AMOUNTS_ERROR_KEY));
        }
    }

    private void printNetAmounts(List<CurrencyBalance> netAmounts){
        StringBuilder sb = new StringBuilder();
        netAmounts.forEach(cna -> sb.append(cna.getCurrency()+" "+cna.getBalance()+"\n"));
        if(sb.length() != 0)
            System.out.println(String.format(env.getProperty(NET_AMOUNTS_STATEMENT_TEMPLATE_KEY), sb.toString().trim()));
    }
}
