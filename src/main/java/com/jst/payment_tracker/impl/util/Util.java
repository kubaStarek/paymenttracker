package com.jst.payment_tracker.impl.util;

import java.util.Optional;

public class Util {
    public static Optional<String[]> parsePaymentLine(String line) {
        return (line.matches("[a-zA-Z]{3}\\s[-]?[1-9][0-9]*"))
                ? Optional.of(line.split("\\s"))
                : Optional.empty();
    }
}
