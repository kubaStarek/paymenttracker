package com.jst.payment_tracker.api.dao;

import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.dao.entity.Payment;
import com.jst.payment_tracker.api.excepion.DAOException;

import java.util.List;

public interface PaymentDAO {

    void executeSinglePayment(Payment payment) throws DAOException;

    void executePaymentBulk(List<Payment> payments) throws DAOException;

    List<CurrencyBalance> getNetAmounts() throws DAOException;
}
