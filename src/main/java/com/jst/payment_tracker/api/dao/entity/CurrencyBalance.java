package com.jst.payment_tracker.api.dao.entity;

public class CurrencyBalance {
    private String currency;

    private long balance;

    public CurrencyBalance(String currency, long balance) {
        this.currency = currency;
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public long getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "CurrencyBalance{" +
                "currency='" + currency + '\'' +
                ", balance=" + balance +
                '}';
    }
}
