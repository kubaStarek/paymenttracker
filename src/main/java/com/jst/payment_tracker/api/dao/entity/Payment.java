package com.jst.payment_tracker.api.dao.entity;

public class Payment {

    private String currency;

    private long amount;

    public Payment(String currency, long amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return currency + " " +amount;
    }
}
