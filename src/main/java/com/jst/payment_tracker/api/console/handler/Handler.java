package com.jst.payment_tracker.api.console.handler;

public interface Handler<Request> {
    void setNextHandler(Handler<Request> handler);
    void process(Request request);
}
