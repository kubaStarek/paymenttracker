package com.jst.payment_tracker.api.console.processor;

public interface ArgsProcessor {
    void process(String... args);
}
