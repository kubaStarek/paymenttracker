package com.jst.payment_tracker.api.console;

public interface ConsoleApp {
    void run(String... args);
}
