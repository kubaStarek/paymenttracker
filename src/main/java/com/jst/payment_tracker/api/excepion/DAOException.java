package com.jst.payment_tracker.api.excepion;

public class DAOException extends Exception {

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
