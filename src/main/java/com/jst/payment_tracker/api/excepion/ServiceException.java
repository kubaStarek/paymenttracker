package com.jst.payment_tracker.api.excepion;


public class ServiceException extends Exception {

    private ErrorStatus errorStatus;

    public ServiceException(ErrorStatus errorStatus) {
        super();
        this.errorStatus = errorStatus;
    }

    public ServiceException(ErrorStatus errorStatus, String message, Throwable cause) {
        super(message, cause);
        this.errorStatus = errorStatus;
    }

    public ErrorStatus getErrorStatus() {
        return errorStatus;
    }

    public enum ErrorStatus {
        INVALID_INPUT,
        INVALID_CURRENCY,
        INVALID_INPUT_FILE,
        GENERAL_ERROR;
    }
}
