package com.jst.payment_tracker.api.service;

import com.jst.payment_tracker.api.dao.entity.CurrencyBalance;
import com.jst.payment_tracker.api.excepion.ServiceException;

import java.util.List;

public interface PaymentService {

    void executeSinglePayment(String currency, long amount) throws ServiceException;

    void importPaymentFile(String filename) throws ServiceException;

    List<CurrencyBalance> getNetAmounts() throws ServiceException;

}
